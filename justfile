set export
set positional-arguments

name := "distrobox"
USER := env_var_or_default("CI_DEPLOY_USER", "rsprta")
PASSWORD := env_var_or_default("CI_DEPLOY_PASSWORD", "none")
REGISTRY := env_var_or_default("CI_REGISTRY", "docker.io")
REPOSITORY := env_var_or_default("CI_REGISTRY_IMAGE", USER + "/" + name)

BUILD_DATE := `date -u +'%Y-%m-%dT%H:%M:%SZ'`
VCS_REF := `git describe --tags --always --dirty`


defaults:
    @just --list

build version platform:
    #!/usr/bin/env sh
    cd images
    buildah build \
        --label "org.opencontainers.image.created=${BUILD_DATE}" \
        --label "org.opencontainers.image.revision=${VCS_REF}" \
        --platform {{platform}} \
        --tag {{REPOSITORY}}:{{version}} \
        --tag {{REPOSITORY}} \
        .

upload version:
    buildah push --creds {{USER}}:{{PASSWORD}} {{REPOSITORY}}:{{version}}
