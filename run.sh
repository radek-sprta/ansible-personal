#! /usr/bin/env bash

# Install script for my personal Ansible playbook
#
# Author: Radek Sprta

# Stop unpredictible behavior
set -o errexit          # Exit on most errors
set -o nounset          # Disallow expansion of unset variables
set -o pipefail         # Use last non-zero exit code in a pipeline

# Define constants
readonly PROJECTS_DIR="Projects/ansible-personal"
readonly PLAYBOOK_REPO="https://gitlab.com/radek-sprta/ansible-personal.git"
readonly PLAYBOOK="setup.yml"
readonly RELEASE_INFO="/etc/os-release"

# Clone the Ansible playbook repository
function clone_repository() {
    mkdir -p "${PROJECTS_DIR}"
    if ! [[ -d "${PROJECTS_DIR}" ]]; then
        eval "${git_command} clone ${PLAYBOOK_REPO} ${PROJECTS_DIR} --force" 
    fi
}

# Display error message
function error() {
     echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $*" >&2
}

# Check if binary is available
function has() {
    command -v "$1" > /dev/null
}

# Initialize OS-specific variables
function initialize_os_vars() {
    # Get OS info
    source "${RELEASE_INFO}"

    case "${ID}" in
        aeon)
            distrobox="true"
            git_command="distrobox enter -- git"
            ;;
        ubuntu)
            distrobox="false"
            git_command="git"
            ;;
    esac
}

# Install Git and Ansible
function install_dependencies() {
    if [ "${distrobox}" == "true" ]; then
        sudo transactional-update pkg install -y ansible git
        sudo transactional-update apply
    else
        sudo apt install -y git ansible
    fi
}

function main() {
    initialize_os_vars
    host="${1:-$(hostname --short)}"

    if ! has ansible || ! has git; then
        install_dependencies
    fi

    cd "${HOME}"
    if ! [ -d "${PROJECTS_DIR}" ]; then
        clone_repository
    fi
    ansible-galaxy install -r "${PROJECTS_DIR}/requirements.yml"
    ansible-playbook -i "${PROJECTS_DIR}/hosts/${host}.yml" \
        "${PROJECTS_DIR}/${PLAYBOOK}" \
        --ask-become-pass \
        --tags all
}

# Run the script
main "$@"

# vim: syntax=sh cc=80 tw=79 ts=4 sw=4 sts=4 et sr
