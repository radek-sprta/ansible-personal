Ansible playbook to setup my personal machines. 

## Usage
`wget -qO- https://gitlab.com/radek-sprta/ansible-personal/raw/master/run.sh | bash`

or to just run the playbook itself after installing dependencies:

```
ansible-galaxy install -r requirements.yml
ansible-playbook setup.yml -i hosts/$(hostname -s).yml --ask-become-pass --tags all
```
